package com.sumskoy.scala.web.repository

import com.sumskoy.scala.web.domain.Person
import org.springframework.stereotype.Repository


@Repository
class PersonRepositoryDefault extends PersonRepository{
  override def findAll(): List[Person] = {
    List(Person("Andrew", "Sumskoy"), Person("Ivan", "Ivanov"))
  }
}

trait PersonRepository {
  def findAll(): List[Person]
}