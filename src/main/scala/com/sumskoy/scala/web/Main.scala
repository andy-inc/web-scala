package com.sumskoy.scala.web

object Main extends App {
  val server = new GenericServerRunner(12, 8080)
  server.execute()
  server.waitForExit()
}
