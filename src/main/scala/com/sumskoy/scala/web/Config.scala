package com.sumskoy.scala.web

import org.springframework.context.annotation.{Configuration, ComponentScan}

@Configuration
@ComponentScan(basePackages = Array("com.sumskoy.scala.web"))
class Config {

}
