package com.sumskoy.scala.web

import org.eclipse.jetty.server.{Connector, Server, ServerConnector}
import org.eclipse.jetty.util.thread.QueuedThreadPool
import org.eclipse.jetty.webapp.WebAppContext
import org.springframework.core.io.ClassPathResource

class GenericServerRunner {
  private var server: Server = null

  def this(maxThreads: Int, port: Int) {
    this()
    server = new Server(new QueuedThreadPool(maxThreads))
    val connector: ServerConnector = new ServerConnector(server)
    connector.setPort(port)
    connector.setSoLingerTime(0)
    server.setConnectors(Array[Connector](connector))
  }

  def execute() = {
    val wardir: String = new ClassPathResource("/webapp/").getURI.toString
    val webContext: WebAppContext = new WebAppContext
    webContext.setContextPath("/")
    webContext.setWar(wardir)
    webContext.setDescriptor(wardir + "WEB-INF/web.xml")
    webContext.setParentLoaderPriority(true)
    server.setHandler(webContext)
    server.start()
  }

  def waitForExit() = {
    server.join()
  }

  def stop() = {
    server.stop()
  }
}
