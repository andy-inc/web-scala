package com.sumskoy.scala.web.domain

case class Person(name: String, surname: String)
