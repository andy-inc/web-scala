package com.sumskoy.scala.web.service

import com.sumskoy.scala.web.domain.Person
import com.sumskoy.scala.web.repository.PersonRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


trait PersonService {
  def findAll(): List[Person]
}

@Service
class PersonServiceDefault @Autowired() (private val personRepository: PersonRepository) extends PersonService {
  override def findAll(): List[Person] = personRepository.findAll()
}
