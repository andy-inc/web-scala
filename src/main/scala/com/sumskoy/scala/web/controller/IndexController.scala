package com.sumskoy.scala.web.controller

import com.sumskoy.scala.web.domain.Person
import com.sumskoy.scala.web.service.PersonService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class IndexController @Autowired() (private val personService: PersonService) {

  def persons(): List[Person] = personService.findAll()

}
