organization := "com.sumskoy"

name := "web-scala"

version := "0.1"

scalaVersion := "2.10.4"

libraryDependencies ++= {
  val jettyV = "9.1.3.v20140225"
  Seq(
    //Servlet
    "org.eclipse.jetty" % "jetty-webapp" % jettyV % "container",
    "org.eclipse.jetty" % "jetty-plus"   % jettyV % "container",
    "org.eclipse.jetty" % "jetty-server"   % jettyV,
    "org.eclipse.jetty" % "jetty-webapp" % jettyV,
    "javax.servlet" % "javax.servlet-api" % "3.0.1" % "provided",
    //Spring IoC
    "org.springframework" % "spring-core" % "4.0.5.RELEASE",
    "org.springframework" % "spring-context" % "4.0.5.RELEASE",
    "org.springframework" % "spring-context-support" % "4.0.5.RELEASE",
    "org.springframework" % "spring-beans" % "4.0.5.RELEASE",
    "org.springframework" % "spring-web" % "4.0.5.RELEASE"
  )
}

Seq(webSettings: _*)
    